//
//  APIHandler.swift
//  Task6 MVC Modal
//
//  Created by Sierra 4 on 28/02/17.
//  Copyright © 2017 Codebrew. All rights reserved.
//

import Foundation
import SwiftyJSON

enum ResponseKeys : String {
    case user = "user"
    case countries = "countries"
}

extension LoginEndpoint {
    
    func handle(parameters : JSON) -> AnyObject? {
        
        switch self {
        case .login(_),.signup(_):
            
            do {
                return try User(attributes: parameters[ResponseKeys.user.rawValue].dictionaryValue )
            } catch _ { return nil }
            
        }
    }
}

