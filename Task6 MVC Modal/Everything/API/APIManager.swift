//
//  APIManager.swift
//  Task6 MVC Modal
//
//  Created by Sierra 4 on 28/02/17.
//  Copyright © 2017 Codebrew. All rights reserved.
//

import Foundation
import SwiftyJSON

class APIManager : NSObject{
    
    typealias Completion = (Response) -> ()
    static let shared = APIManager()
    private lazy var httpClient : HTTPClient = HTTPClient()
    
    func request(with api : Router , completion : @escaping Completion )  {
        
        httpClient.postRequest(withApi: api, success: {[weak self] (data) in
            guard let response = data else {
                completion(Response.failure(.none))
                return
            }
            let json = JSON(response)
            print(json)
            
            let responseType = Validate(rawValue: json[APIConstants.message].stringValue) ?? .failure
            
            if responseType == Validate.success{
                
                let object : AnyObject?
                object = api.handle(parameters: json)
                completion(Response.success(object))
                
                return
            }
            else if responseType == Validate.failPswd {
                completion(Response.failure(.failPswd))
            } else if responseType == Validate.failUsr {
                completion(Response.failure(.failUsr))
            }
            else{  completion(Response.failure(.failure)) }
            
            }, failure: {[weak self] (message) in
                completion(Response.failure(.failure))
                
        })
        
    }
}
