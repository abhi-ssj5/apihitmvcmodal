//
//  LoginEndPoint.swift
//  Task6 MVC Modal
//
//  Created by Sierra 4 on 28/02/17.
//  Copyright © 2017 Codebrew. All rights reserved.
//

import Foundation
import Alamofire

enum LoginEndpoint {
    
    case login(email : String? , password : String?, flag: String?)
    case signup(username: String?, email: String?, password: String?, phone: String?, country: String?, city: String?, birthday: String?, flag: String?, state: String?, address: String?, country_code:String?, country_iso3: String?)
    
}


extension LoginEndpoint : Router{
    
    var route : String  {
        switch self {
        case .login(_): return APIConstants.login
        case .signup(_): return APIConstants.signup
        }
    }
    
    var parameters: OptionalDictionary{
        return format()
    }
    
    
    func format() -> OptionalDictionary {
        
        switch self {
            
        case .login(let email , let password, let flag):
            return Parameters.login.map(values: [email,password,flag])
            
        case .signup(let username, let email, let password, let phone, let country, let city, let birthday, let flag, let state, let address, let country_code, let country_iso3):
            return Parameters.signup.map(values: [username, email, password, phone, country, city, birthday, flag, state, address, country_code, country_iso3])
        }
    }
    
    var method : Alamofire.HTTPMethod {
        switch self {
        default:
            return .post
        }
    }
    
    var baseURL: String{
        return APIConstants.basePath
    }
    
}
