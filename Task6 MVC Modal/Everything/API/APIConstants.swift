//
//  APIConstants.swift
//  Task6 MVC Modal
//
//  Created by Sierra 4 on 28/02/17.
//  Copyright © 2017 Codebrew. All rights reserved.
//

import Foundation

internal struct APIConstants {
    
    static let basePath = "http://34.195.206.185/api/"
    static let status_code = "status_code"
    static let message = "msg"
    static let login = "login"
    static let signup = "signup"
}

enum Keys : String{
    
    //User LoginProcess Keys
    case email = "email"
    case password = "password"
    case flag = "flag"
    
    //SignUp Keys
    case username = "username"
    case phone = "phone"
    case country = "country"
    case city = "city"
    case address = "address"
    case birthday = "birthday"
    case country_code = "country_code"
    case country_iso3 = "country_iso3"
    case state = "state"
}

enum Validate : String {
    
    case none
    case success = "User logged in successfully.!"
    case failPswd = "Password Incorrect!"
    case failUsr = "User is not Registered!"
    case failure = "400"
    case failemail = "email already exist"
    
    func map(response message : String?) -> String? {
        
        switch self {
        case .success:
            return message
            
        case .failPswd:
            return message
            
        case .failure:
            return message
            
        case .failUsr:
            return message
            
        case .failemail:
            return message
            
        default:
            return nil
        }
        
    }
}

enum Response {
    case success(AnyObject?)
    case failure(Validate)
}

typealias OptionalDictionary = [String : String]?

struct Parameters {
    
    static let login : [Keys] = [.email, .password, .flag]
    
    static let signup : [Keys] = [.username, .email, .password, .phone, .country, .city, .birthday, .flag, .state, .address, .country_code, .country_iso3]
}
