//
//  SignUpViewController.swift
//  Task6 MVC Modal
//
//  Created by Sierra 4 on 28/02/17.
//  Copyright © 2017 Codebrew. All rights reserved.
//

import UIKit
import SkyFloatingLabelTextField
import ISMessages

class SignUpViewController: BaseViewController {
    
    @IBOutlet weak var txtUserName: SkyFloatingLabelTextField!
    @IBOutlet weak var txtEmail: SkyFloatingLabelTextField!
    @IBOutlet weak var txtPasswd: SkyFloatingLabelTextField!
    @IBOutlet weak var txtPhone: SkyFloatingLabelTextField!
    @IBOutlet weak var txtCountry: SkyFloatingLabelTextField!
    @IBOutlet weak var txtCity: SkyFloatingLabelTextField!
    @IBOutlet weak var txtAddress: SkyFloatingLabelTextField!
    @IBOutlet weak var btnSignUp: UIButton!
    @IBOutlet weak var btnBack: UIButton!

    override func viewDidLoad() {
        super.viewDidLoad()
        
        //text field properties
        txtUserName.selectedTitleColor = UIColor(red: 0.71 ,green: 0.71 ,blue: 0.71 , alpha: 1)
        txtUserName.selectedLineColor = UIColor(red: 0.0 ,green: 0.71 ,blue: 0.95 , alpha: 1)
        
        txtEmail.selectedTitleColor = UIColor(red: 0.71 ,green: 0.71 ,blue: 0.71 , alpha: 1)
        txtEmail.selectedLineColor = UIColor(red: 0.0 ,green: 0.71 ,blue: 0.95 , alpha: 1)
        
        txtPasswd.selectedTitleColor = UIColor(red: 0.71 ,green: 0.71 ,blue: 0.71 , alpha: 1)
        txtPasswd.selectedLineColor = UIColor(red: 0.0 ,green: 0.71 ,blue: 0.95 , alpha: 1)
        
        txtPhone.selectedTitleColor = UIColor(red: 0.71 ,green: 0.71 ,blue: 0.71 , alpha: 1)
        txtPhone.selectedLineColor = UIColor(red: 0.0 ,green: 0.71 ,blue: 0.95 , alpha: 1)
        
        txtCountry.selectedTitleColor = UIColor(red: 0.71 ,green: 0.71 ,blue: 0.71 , alpha: 1)
        txtCountry.selectedLineColor = UIColor(red: 0.0 ,green: 0.71 ,blue: 0.95 , alpha: 1)
        
        txtCity.selectedTitleColor = UIColor(red: 0.71 ,green: 0.71 ,blue: 0.71 , alpha: 1)
        txtCity.selectedLineColor = UIColor(red: 0.0 ,green: 0.71 ,blue: 0.95 , alpha: 1)
        
        txtAddress.selectedTitleColor = UIColor(red: 0.71 ,green: 0.71 ,blue: 0.71 , alpha: 1)
        txtAddress.selectedLineColor = UIColor(red: 0.0 ,green: 0.71 ,blue: 0.95 , alpha: 1)
        
        //sign up button
        self.btnSignUp.layer.cornerRadius = btnSignUp.frame.height / 2
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    @IBAction func backAction(_ sender: Any) {
        _ = navigationController?.popViewController(animated: true)
    }
    
    func Validate() -> Valid{
        let value = Validation.shared.validateSignUp(signup: txtUserName.text, email: txtEmail.text, password: txtPasswd.text, phone: txtPhone.text, country: txtCountry.text, city: txtCity.text, address: txtAddress.text)
        return value
    }
    
    //MARK: Handle Response
    func handleResponse(response : Response) {
        switch response{
        case .success(let responseValue):
            if let _ = responseValue as? User{
                Alerts.shared.show(alert: .success, message: Alert.signup.rawValue, type: .success)
            }
        case .failure(let str):
            Alerts.shared.show(alert: .oops, message: /str.rawValue, type: .error)
        }
        btnSignUp.isEnabled = true
        btnBack.isEnabled = true
    }
}

extension SignUpViewController {
    
    @IBAction func signUpAction(_ sender: Any) {
        print("signup clicked")
        btnSignUp.isEnabled = false
        btnBack.isEnabled = false
        ISMessages.hideAlert(animated: true)
        let value = Validate()
        switch value {
        case .success:
            APIManager.shared.request(with: LoginEndpoint.signup(username: txtUserName.text, email: txtEmail.text, password: txtPasswd.text, phone: txtPhone.text, country: txtCountry.text, city: txtCity.text, birthday: "12/10/1994", flag: "1", state: "chandigarh", address: txtAddress.text, country_code: "001", country_iso3: "blehh..."), completion: {[weak self] (response) in
                self?.handleResponse(response: response)})
            
        case .failure(let title,let msg):
            Alerts.shared.show(alert: title, message: msg , type : .info)
            btnSignUp.isEnabled = true
            btnBack.isEnabled = true
        }
    }
    
    
}









