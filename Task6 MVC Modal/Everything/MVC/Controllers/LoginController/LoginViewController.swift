//
//  ViewController.swift
//  Task6 MVC Modal
//
//  Created by Sierra 4 on 28/02/17.
//  Copyright © 2017 Codebrew. All rights reserved.
//

import UIKit
import SkyFloatingLabelTextField
import M13Checkbox
import ISMessages

class ViewController: BaseViewController {

    @IBOutlet weak var txtEmail: SkyFloatingLabelTextField!
    @IBOutlet weak var txtPasswd: SkyFloatingLabelTextField!
    @IBOutlet weak var checkBox: M13Checkbox!
    @IBOutlet weak var btnSignUp: UIButton!
    @IBOutlet weak var btnSignIn: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //textField properties
        //username textfield
        txtEmail.selectedTitleColor = UIColor(red: 0.0 ,green: 0.71 ,blue: 0.95 , alpha: 1)
        txtEmail.lineHeight = 0
        txtEmail.selectedLineHeight = 0
        
        //password textfield
        txtPasswd.selectedTitleColor = UIColor(red: 0.0 ,green: 0.71 ,blue: 0.95 , alpha: 1)
        txtPasswd.lineHeight = 0
        txtPasswd.selectedLineHeight = 0
        
        //chcekBox
        checkBox._IBBoxType = "Square"
        checkBox.cornerRadius = 0
        checkBox.stateChangeAnimation = M13Checkbox.Animation(rawValue: "Fill")!
        
        //sign in button
        btnSignIn.layer.cornerRadius = 5
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func Validate() -> Valid{
        let value = Validation.shared.validate(login: txtEmail?.text, password: txtPasswd?.text)
        return value
    }
    
    //MARK: Handle Response
    func handleResponse(response : Response) {
        switch response{
        case .success(let responseValue):
            if let _ = responseValue as? User{
                Alerts.shared.show(alert: .success, message: Alert.login.rawValue, type: .success)
            }
            //self.performSegue(withIdentifier: "LoginSuccess", sender: nil)
        case .failure(let str):
            Alerts.shared.show(alert: .oops, message: /str.rawValue, type: .error)
        }
        btnSignIn.isEnabled = true
        btnSignUp.isEnabled = true
    }
}

extension ViewController {
    
    @IBAction func signInAction(_ sender: Any) {
        print("sign in clicked")
        btnSignIn.isEnabled = false
        btnSignUp.isEnabled = false
        ISMessages.hideAlert(animated: true)
        let value = Validate()
        switch value {
        case .success:
            APIManager.shared.request(with: LoginEndpoint.login(email: txtEmail.text, password: txtPasswd.text, flag: "1"), completion: {[weak self] (response) in
                self?.handleResponse(response: response)})
            
        case .failure(let title,let msg):
            Alerts.shared.show(alert: title, message: msg , type : .info)
            btnSignIn.isEnabled = true
            btnSignUp.isEnabled = true
        }
    }
    
    @IBAction func signUpAction(_ sender: Any) {
        performSegue(withIdentifier: "SignUp", sender: nil)
    }
    
}










