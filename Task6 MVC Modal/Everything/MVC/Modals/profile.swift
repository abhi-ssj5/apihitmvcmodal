//
//  profile.swift
//  Task6 MVC Modal
//
//  Created by Sierra 4 on 28/02/17.
//  Copyright © 2017 Codebrew. All rights reserved.
//

import Foundation
import SwiftyJSON

class Profile: NSObject {
    var username: String?
    var city: String?
    var password: String?
    var language: String?
    var address: String?
    var email: String?
    var phone: String?
    var flag: String?
    var birthday: String?
    var country: String?
    
    required init(attributes: OptionalJSON) throws{
        super.init()
        username = .username => attributes
        city = .city => attributes
        password = .password => attributes
        address = .address => attributes
        email = .email => attributes
        phone = .phone => attributes
        flag = .flag => attributes
        birthday = .birthday => attributes
        country = .country => attributes
    }
    
    override init() {
        super.init()
    }
    
}
