//
//  user.swift
//  Task6 MVC Modal
//
//  Created by Sierra 4 on 28/02/17.
//  Copyright © 2017 Codebrew. All rights reserved.
//

import Foundation
import SwiftyJSON

class User: NSObject {
    var msg: String?
    var status_code: String?
    var profile: Profile?
    
    required init(attributes: OptionalJSON) throws{
        super.init()
        msg = .msg => attributes
        status_code = .status_code => attributes
        profile = try Profile(attributes: .profile =< attributes)
    }
    
    override init() {
        super.init()
    }
}
