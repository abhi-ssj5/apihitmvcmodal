//
//  ParamKeys.swift
//  Task6 MVC Modal
//
//  Created by Sierra 4 on 28/02/17.
//  Copyright © 2017 Codebrew. All rights reserved.
//

import Foundation

enum ParamKeys: String {
    
    case username = "username"
    case city = "city"
    case password = "password"
    case access_token = "access_token"
    case address = "address"
    case email = "email"
    case phone = "phone"
    case flag = "flag"
    case birthday = "birthday"
    case country = "country"
    case msg = "msg"
    case status_code = "status_code"
    case profile = "profile"
    
}
