//
//  validation.swift
//  Task6 MVC Modal
//
//  Created by Sierra 4 on 28/02/17.
//  Copyright © 2017 Codebrew. All rights reserved.
//

import Foundation

enum Valid{
    case success
    case failure(Alert,String)
}

class Validation: NSObject {
    
    static let shared = Validation()
    
    func validate(login email : String? , password : String?) -> Valid {
        
        if (/email).isEmpty {
            return errorMsg(str: "Please enter your email id")
        }
        else if (/password).isEmpty{
            return errorMsg(str: "Please enter password")
        }
        return .success
    }
    
    func validateSignUp(signup username: String?, email: String?, password: String?, phone: String?, country: String?, city: String?, address: String?) -> Valid {
        
        if (/username).isEmpty {
            return errorMsg(str: "Please enter your username")
        }
        
        if (/email).isEmpty {
            return errorMsg(str: "Please enter your email id")
        }
            
        if (/password).isEmpty {
            return errorMsg(str: "Please enter password")
        }
        
        if (/phone).isEmpty {
            return errorMsg(str: "Please enter your phone no")
        }
        
        if !(/phone).isEmpty {
            let num = /phone
            let numbers = CharacterSet.decimalDigits
            if String(num.characters[num.characters.startIndex]) == "0" {
                return errorMsg(str: "Invalid phone no")
            }
            if num.characters.count > 10 || num.characters.count < 10 {
                return errorMsg(str: "Invalid phone no")
            }
            for number in num.unicodeScalars {
                if !numbers.contains(number) {
                    return errorMsg(str: "Invalid phone no")
                }
            }
        }
        
        if (/country).isEmpty {
            return errorMsg(str: "Please enter your country")
        }
        if (/city).isEmpty {
            return errorMsg(str: "Please enter your city")
        }
        if (/address).isEmpty {
            return errorMsg(str: "Please enter your address")
        }
        
        return .success
    }
    
    
    func errorMsg(str : String) -> Valid{
        return .failure(.error,str)
    }
    
}
